#!/bin/bash

# m4_ignore(
echo "This is just a script template, not the script (yet) - pass it to 'argbash' to fix this." >&2
exit 11  #)Created by argbash-init v2.8.1
# ARG_OPTIONAL_BOOLEAN([quiet],[],[shhh, be quiet])
# ARG_POSITIONAL_SINGLE([verb],[What to do, one of start, stop, reset (stop, delete config, fresh start), getkc (get kubeconfig to a file), check; default is check],[check])
# ARG_DEFAULTS_POS
# ARG_OPTIONAL_SINGLE([service], s, [The service name, default k3s.service ], [k3s.service])
# ARG_OPTIONAL_SINGLE([dir], d, [The directory name, default /var/lib/rancher ], [/var/lib/rancher])
# ARG_OPTIONAL_SINGLE([kubeconfigfile], k, [The kubeconfig file name, default k3s-kubeconfig ], [k3s-kubeconfig])
# ARG_OPTIONAL_BOOLEAN([force-start],[],[After resetting start the service, even if it was not running in first place])
# ARG_HELP([Helping you to start, stop and reser your k3s])
# ARGBASH_GO

# [ <-- needed because of Argbash

STARTED="no"
FORBIDDEN_DIRS="/,/home,/boot,/etc"

say () {
	if [ "$_arg_quiet" = "off" ];
	then
		printf "%s\\n" "$1"
	fi
}

start () {
	say "Start the service"
	[ ! "$1" = "force" ] &&  check
	if [ "$STARTED" = "yes" ];
	then
		say "Service already running, Forrest, nothing to do."
	else
		say "Starting service..."
		sudo systemctl start $_arg_service
		check
		if [ "$STARTED" = "yes" ];
		then
			say "Service started ok"
		else
			say "Service is not running, Houston we have a problem...:( "
		fi
	fi
}

stop () {
	say "Stop the service"
	[ ! "$1" = "force" ] &&  check
	if [ "$STARTED" = "no" ];
	then
		say "Service is not running, nothing to do."
	else
		say "Stopping service..."
		sudo systemctl stop $_arg_service
		check
		if [ "$STARTED" = "no" ];
		then
			say "Service stopped ok"
		else
			say "Wow, dude, service is still running, what's uuuuuup?...:( "
		fi
	fi
}

check () {
	say "Check the service"
	STARTED="no"
	sudo systemctl is-active --quiet $_arg_service && STARTED="yes" 
}

get-kubeconfig () {
	say "Get the kubeconfig file"
	say "Sending output to $_arg_kubeconfigfile"
	sudo k3s kubectl config view --raw > $_arg_kubeconfigfile
	say "Done, now you can run this command: export KUBECONFIG=$(readlink -f $_arg_kubeconfigfile)"
}

reset () {
	say "Reset environment"
	
	for d in $(echo "$FORBIDDEN_DIRS" | tr "," " ");
	do
		if [ "$_arg_dir" = "$d" ];
		then
			die "Hye, yo, this directoy is forbidden!" 1
		fi
	done

	if [ -d $_arg_dir ];
	then
		check
		local IS_RUNNING=$STARTED
		
		if [ "$IS_RUNNING" = "yes" ];
		then
			say "Step 0: stop the service"
			stop force
		fi
		say "Step 1: delete directory $_arg_dir"
		cmd="sudo rm -rf $_arg_dir""/*"
		eval $cmd
		if [ "$IS_RUNNING" = "yes" ] || [ "$_arg_force_start" = "on" ];
		then
			say "Step 2: start the service"
			start force
		fi
	else
		die "We have a problem reading dir $_arg_dir, please specify a dir with no wildcards, e.g. /var/lib/rancher" 1
	fi

}


say "* ********************************** *"
say "* K3S boot script                    *"
say "* ********************************** *"
say " "

say "* ---------------- *"
say "* Taking action    *"
say " "

case $_arg_verb in
	start)
		start
		get-kubeconfig
		;;
	stop)
		stop
		;;
	reset)
		reset
		get-kubeconfig
		;;
	getkc)
		get-kubeconfig
		;;
	*)
		_PRINT_HELP="yes"
		die "Verb $_arg_verb does not exist!" 1
		;;
esac

say "Process finished."

# ] <-- needed because of Argbash
